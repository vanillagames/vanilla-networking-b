﻿//NetworkingMain.cs
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
 
public class NetworkMain : MonoBehaviour {
 
   //Enum
   enum NetworkType {
     undefined,
     server,
     client
   }
 
 
 
   //Preference Variables
   public int maxConnections = 10;//Max connections allowed.
   public bool autoStartServer = false;//Will this automatically become a server when the game is started?
 
 
 
   //Script Variables
   int localSocket = -1;//The socket the server or client is connected to.
   int connectionId = -1;//The ID of connection. Used to distinguish different clients on the network.
   bool isInitialized = false;//Is the script currently networked, or offline?
   NetworkType networkType;//State of this game instance. Is it a server, client, or undefined (neither a client or server, yet).
   List<int> activeConnectionIds = new List<int>();//All active connections. Only manager on server.
 
 
 
   //Channels
   byte channelUnreliable;//Unreliable channel.
   byte channelReliable;//Reliable channel.
 
 
 
   //Configuration Variables
   GlobalConfig gc = new GlobalConfig();
   ConnectionConfig cc = new ConnectionConfig();
   HostTopology ht;
 
 
 
 
 
   //--MONOBEHAVIOR METHODS------------------------------------------------------------------------------------------------------------------------------\\
   void Start() {
 
     //Customize global config
     gc.ReactorModel = ReactorModel.FixRateReactor;
     gc.ThreadAwakeTimeout = 10;
 
     //Customize channel config
     channelReliable = cc.AddChannel(QosType.ReliableSequenced);
     channelUnreliable = cc.AddChannel(QosType.UnreliableSequenced);
 
     //Create host topology from config
     ht = new HostTopology(cc, maxConnections);
 
     //Initialize network transport. Allows for the creation/connection of servers.
     NetworkTransport.Init(gc);
 
     //If 'autoStartServer' is true, then start a server.
     if ( autoStartServer )
       StartServer();
 
     StartCoroutine( "TimedMessage" );
   }

   public KeyCode startServerKey;
   public KeyCode joinServerKey;
   public KeyCode disconnectKey;
 
 
   void Update() {
     if (Input.GetKeyDown((startServerKey)))
       StartServer();
     
     if (Input.GetKeyDown((joinServerKey)))
     JoinServer();
     
     if (Input.GetKeyDown(disconnectKey))
       Disconnect();
     
     //Recieve Network Messages
     ReceiveMessages();
   }
 
 
 
 
 
//   void OnGUI() {
//     if ( GUI.Button(new Rect(Screen.width * .5f - 50, Screen.height * .5f - 50, 100, 40), "Create a Server") ) {
//       StartServer();
//     }
//     if ( GUI.Button(new Rect(Screen.width * .5f - 50, Screen.height * .5f + 50, 100, 40), "Join a Server") ) {
//       JoinServer();
//     }
//     if ( GUI.Button(new Rect(Screen.width * .5f - 50, Screen.height * .5f + 150, 100, 40), "Disconnect") ) {
//       Disconnect();
//     }
//   }
 
 
 
 
 
   //--START/CONNECT SERVER------------------------------------------------------------------------------------------------------------------------------\\
   public void StartServer(int port = 25565) {
     //Make sure this game is not already a server or client
     if ( networkType != NetworkType.undefined ) {
       Debug.LogError( "Error: This game is already a server or client. Cannot make it a server." );
       Debug.Log("Error: This game is already a server or client. Cannot make it a server.");
     }
 
     //Open socket for server
     localSocket = NetworkTransport.AddHost(ht, port);
 
     //Make sure the socket formations were successful
     if ( localSocket < 0 ) {
       Debug.Log("Server socket creation failed!");
     } else {
       Debug.Log("Server socket creation successful! Socket: " + localSocket);
       networkType = NetworkType.server;
     }
 
     isInitialized = true;
   }
 
 
 
 
 
   public void JoinServer(string ip = "127.0.0.1", int port = 25565) {
     //Make sure this game is not already a server or client
     if ( networkType != NetworkType.undefined ) {
       Debug.LogError("Error: This game is already a server or client. Cannot make it a client.");
       Debug.Log("Error: This game is already a server or client. Cannot make it a client.");
     }
 
     //Open socket for client
     localSocket = NetworkTransport.AddHost(ht);
 
     //Make sure the socket formations were successful
     if ( localSocket < 0 ) {
       Debug.Log("Client socket creation failed!");
     } else {
       Debug.Log("Client socket creation successful! Socket: " + localSocket);
       networkType = NetworkType.client;
     }
 
     isInitialized = true;
 
     //Connect to server
     byte error;
     connectionId = NetworkTransport.Connect(localSocket, ip, port, 0, out error);
     LogNetworkError(error);
   }
 
 
 
 
 
   //--SEND DATA------------------------------------------------------------------------------------------------------------------------------\\
   public void ServerSendDataToSingle(string message, int recipient) {//message, (send to single) int id
     List<int> recipients = new List<int>();
     recipients.Add(recipient);
     ServerSendDataInner(message, recipients);
   }
   public void ServerSendDataToMultiple(string message, List<int> recipients) {//message, (send to multiple) list<int> ids
     ServerSendDataInner(message, recipients);
   }
   public void ServerSendDataToAll(string message) {//message, (send to all)
     ServerSendDataInner(message, activeConnectionIds);
   }
   public void ServerSendDataInner(string message, List<int> recipients) {
     //If not server, return
     if ( networkType != NetworkType.server ) {
       if ( networkType == NetworkType.undefined ) {
         Debug.LogError("Error: Cannot sent message. No active connections");
       } else {
         Debug.LogError("Error: Client cannot send messages with this method. Use [ClientSendDataToHost] instead.");
       }
       return;
     }
 
     // Send the client(s) a message
     byte error;
     byte[] buffer = new byte[1024];
     Stream stream = new MemoryStream(buffer);
     BinaryFormatter f = new BinaryFormatter();
     f.Serialize(stream, message);
 
     foreach ( int id in recipients ) {
       NetworkTransport.Send(localSocket, id, channelReliable, buffer, ( int ) stream.Position, out error);
       LogNetworkError(error);
     }
   }
 
 
 
 
 
   public void ClientSendDataToHost(string message) {
     //If not client, return
     if ( networkType != NetworkType.client ) {
       if ( networkType == NetworkType.undefined ) {
         Debug.LogError("Error: Cannot sent message. No active connections");
       } else {
         Debug.LogError("Error: Server cannot send messages with this method. Use a variation of [ServerSendData___] instead.");
       }
       return;
     }
 
     // Send the server a message
     byte error;
     byte[] buffer = new byte[1024];
     Stream stream = new MemoryStream(buffer);
     BinaryFormatter f = new BinaryFormatter();
     f.Serialize(stream, message);
 
     NetworkTransport.Send(localSocket, connectionId, channelReliable, buffer, ( int ) stream.Position, out error);
     LogNetworkError(error);
   }
 
 
 
 
 
   //--RECEIVE DATA------------------------------------------------------------------------------------------------------------------------------\\
   void ReceiveMessages() {
     //Not a server or a connected client, return
     if ( !isInitialized ) {
       return;
     }
 
     int recHostId;
     int connId;
     int channelId;
     int dataSize;
     byte[] buffer = new byte[1024];
     byte error;
 
     NetworkEventType networkEvent = NetworkEventType.DataEvent;
 
     do {
       networkEvent = NetworkTransport.Receive(out recHostId, out connId, out channelId, buffer, 1024, out dataSize, out error);
 
       switch ( networkEvent ) {
         case NetworkEventType.Nothing:
           break;
 
 
 
         case NetworkEventType.ConnectEvent:
           if ( networkType == NetworkType.server ) {
             Debug.Log("Server: Player " + connId.ToString() + " connected!");
             activeConnectionIds.Add(connId);
             ServerSendDataToSingle("You connected to me!", connId);
             ServerSendDataToAll("A new player connected!");
           } else if ( networkType == NetworkType.client ) {
             Debug.Log("Client: Client connected to " + connId.ToString() + "!");
 
             ClientSendDataToHost("I connected to you!");
           }
           break;
 
 
 
         case NetworkEventType.DataEvent:
           //Decode data
           Stream stream = new MemoryStream(buffer);
           BinaryFormatter f = new BinaryFormatter();
           string msg = f.Deserialize(stream).ToString();
 
           //Received data
           if ( networkType == NetworkType.server ) {
 
             Debug.Log("Server: Received Data from " + connId.ToString() + "! Message: " + msg);
           } else if ( networkType == NetworkType.client ) {
             Debug.Log("Client: Received Data from " + connId.ToString() + "! Message: " + msg);
           }
           break;
 
 
 
         case NetworkEventType.DisconnectEvent:
           if ( networkType == NetworkType.server && connId != localSocket ) {
             Debug.Log("Server: Received disconnect from " + connId.ToString());
             int toRemove = -1;
             for ( int i = 0; i < activeConnectionIds.Count; i++ ) {
               if ( activeConnectionIds[i] == connId ) {
                 toRemove = i;
                 break;
               }
             }
             activeConnectionIds.RemoveAt(toRemove);
           } else if ( networkType == NetworkType.client ) {
             Debug.Log("Client: Disconnected from server!");
             Disconnect();
           }
           break;
       }
 
     } while ( networkEvent != NetworkEventType.Nothing );
   }
 
 
 
 
 
   //--DISCONNECT------------------------------------------------------------------------------------------------------------------------------\\
   public void Disconnect() {
     if ( networkType == NetworkType.client ) {
       byte error;
       NetworkTransport.Disconnect(localSocket, connectionId, out error);
       LogNetworkError(error);
     }
 
     //Close connection
     NetworkTransport.Shutdown();
 
     //Reset Variables
     localSocket = -1;
     connectionId = -1;
     isInitialized = false;
     networkType = NetworkType.undefined;
     List<int> activeConnectionIds = new List<int>();
     channelUnreliable = 0;
     channelReliable = 0;
     StopCoroutine( "TimedMessage" );
 
     //Re-Open connection
     NetworkTransport.Init(gc);
     StartCoroutine( "TimedMessage" );
   }
 
 
 
 
 
   //--OTHER------------------------------------------------------------------------------------------------------------------------------\\
   void LogNetworkError(byte error) {
     if ( error != ( byte ) NetworkError.Ok ) {
       NetworkError nerror = ( NetworkError ) error;
       Debug.LogError("Error: " + nerror.ToString());
       Debug.Log("Error: " + nerror.ToString());
     }
   }
 
   IEnumerator TimedMessage() {
     if ( networkType == NetworkType.server ) {
       ServerSendDataToAll( "Server to all: connected!" );
     } else if ( networkType == NetworkType.client ) {
       ClientSendDataToHost( "Client to host: connected!" );
     }
     yield return new WaitForSeconds(1);
     StartCoroutine( "TimedMessage" );
   }
 
}