﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine.Networking;

using PHORIA.OurPlanet;

namespace PHORIA.Networking
{

    public abstract class PNetServer : NetworkBehaviour
    {
        #region Data
        
        [EnumFlags(2)]
        public PNetServerEventFlags serverEventFlags;

        public bool usePassword;
        
        public string password = "Fuzzy pickles";

        #endregion
        
        protected sealed override byte[] ReceiveBuffer
        {
            get => _receiveBuffer;
            set => _receiveBuffer = new byte[profile.serverReceiveBufferSize];
        }


        //-------------------------------------------------------------------------------------------------------------/
        // Connecting
        //-------------------------------------------------------------------------------------------------------------/
        
        public virtual void OnEnable()
        {
            if (!initializeOnEnable) return;

            UNet_InitializeNetworking();    
            
            UNet_OpenHostSocket();
        }

        public virtual void OnDisable()
        {
            if (!initializeOnEnable) return;

            UNet_CloseHostSocket();
        }
        
        protected sealed override int UNet_ConfigureHostSocket()
        {
            Log($"{profile.topology.MaxDefaultConnections}");
            Log($"{profile.topology.DefaultConfig.Channels.Count.ToString()}");

            // Servers need to open a socket for communication and it contains more crucial parameters:
            return NetworkTransport.AddHost(profile.topology, profile.localPort, profile.ip);
        }

        //-------------------------------------------------------------------------------------------------------------/
        // Network Traffic
        //-------------------------------------------------------------------------------------------------------------/
        
        protected sealed override void UNet_Connect(int connection)
        {
            if (ConnectionList.Contains(connection))
            {
                Warning($"The client connectionId [{connection}] cannot be added to the " +
                        $"ConnectedClients list because it is already present.");
                return;
            }

            if (usePassword)
            {
                Send_PasswordRequest(connection);
            }
            else
            {
                Send_InitPacket(connection);
            }
            
        }

        protected sealed override void UNet_Disconnect(int connection)
        {
            if (!ConnectionList.Contains(connection))
            {
                Warning($"The following client connectionId cannot be removed from the ConnectedClients list because it isn't present to begin with - [{connection}");
                return;
            }

            StopTrackingClient(connection);
            
            bufferConstructor.Clear();
            
            bufferConstructor.Add((byte)NetOpFromServer.ClientDisconnected);
            bufferConstructor.Add((byte)connection);
                     
            SendToAll(1, bufferConstructor.ToArray());
        }

        protected sealed override void UNet_Data(int connection, byte[] buffer, int dataSize)
        {
            LogPNetEvent(PNetServerEventFlags.AllIncoming, connection, buffer);
            
            switch ((NetOpFromClient) buffer[0])
            {
                case NetOpFromClient.UnconfiguredPacket:
                    LogPNetEvent(PNetServerEventFlags.UnconfiguredPacket, connectionId, buffer);
                    break;
                
                case NetOpFromClient.PasswordSubmission:
                    LogPNetEvent(PNetServerEventFlags.ReceivePasswordAttempt, connection, buffer);
                    ReceivePasswordSubmission(connection, buffer, dataSize);
                    break;
                
                case NetOpFromClient.ClientConnected:
                    LogPNetEvent(PNetServerEventFlags.ReceiveClientConnected, connection, buffer);
                    Receive_ClientConnected(connection, buffer, dataSize);
                    break;

                case NetOpFromClient.ClientDisconnected:
                    LogPNetEvent(PNetServerEventFlags.ReceiveClientDisconnected, connection, buffer);
                    Receive_ClientDisconnected(connection, buffer, dataSize);
                    break;
                
                case NetOpFromClient.Data:
                    Receive_ClientData(connection, buffer, dataSize);
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        protected virtual void ReceivePasswordSubmission(int connection, byte[] buffer, int dataSize)
        {
            string passwordAttempt = Encoding.UTF8.GetString(buffer, 1, dataSize);

            if (string.Equals(passwordAttempt, password))
            {
                Send_InitPacket(connection);
            }
            else
            {
                Send_PasswordDenied(connection);
            }
        }

        protected abstract void Receive_ClientData(int connection, byte[] buffer, int dataSize);
        
        //------------------------------------------------------------------------------------------------------------/
        // Send
        //------------------------------------------------------------------------------------------------------------/

        protected virtual void Send_PasswordRequest(int connection)
        {
            byte[] buffer = {(byte)NetOpFromServer.PasswordRequest};

            LogPNetEvent(PNetServerEventFlags.SendPasswordRequest, connection, buffer);
            
            SendToIndividual(connection, 1, buffer);
        }

        protected virtual void Send_PasswordDenied(int connection)
        {
            byte[] buffer = new byte[1];

            buffer[0] = (byte) NetOpFromServer.PasswordDenied;
            
            SendToIndividual(connection,1, buffer);
        }

        protected virtual void Send_InitPacket(int connection)
        {
            byte[] buffer = new byte[2];

            buffer[0] = (byte) NetOpFromServer.InitPacket;
            buffer[1] = (byte) connection;
            
            LogPNetEvent(PNetServerEventFlags.SendInitPacket, connection, buffer);
            
            SendToIndividual(connection, 1, buffer);
        }
        
        /// <summary>This sends our new and contextual client connection to all connected clients on the network.</summary>
        protected virtual void Send_ClientConnected(int connection, byte clientType, byte[] incomingBuffer, int dataStart)
        {
            bufferConstructor.Clear();
            
            bufferConstructor.Add((byte)NetOpFromServer.ClientConnected);
            bufferConstructor.Add((byte)connection);

            byte[] buffer = bufferConstructor.ToArray();
            
            LogPNetEvent(PNetServerEventFlags.SendClientConnected, connection, buffer);
            
            LogPNetEvent(PNetServerEventFlags.AllOutgoing, connection, buffer);

            SendToAll(1, buffer);
        }
        
        
        //------------------------------------------------------------------------------------------------------------/
        // Receive
        //------------------------------------------------------------------------------------------------------------/

        protected virtual void Receive_ClientConnected(int connection, byte[] buffer, int dataSize)
        {
            LogPNetEvent(PNetServerEventFlags.ReceiveClientConnected, connection, buffer);

            LogPNetEvent(PNetServerEventFlags.StartTrackingConnection, connection, buffer);

            // The server can process this connection here before any of the clients can.
            StartTrackingClient(connection);
            
            // Pass the received buffer on to all the other clients
            SendToAll(1, buffer);
        }
        
        /// <summary>
        /// This method is used as part of the BeginInitializationHandshake() loop. It iterates through all the currently
        /// connected clients and its purpose is to easily replace what kind of information is included about
        /// each of them. By default, it includes each clients connection int and 'clientType' int, but it is strongly
        /// advised to also include each clients current pose and any other crucial information about them.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="currentClientId"></param>
        /// <param name="currentBufferIndex"></param>
        protected virtual void AddClientInfoToInitBuffer(ref byte[] buffer, int currentClientId, int currentBufferIndex)
        {
            buffer[currentBufferIndex] = (byte)ConnectionList[currentClientId];
        }
        
        /// <summary>
        /// This function serves several purposes. Mostly importantly, it updates a new client with its 'real'
        /// connectionId int so it can be properly identified across the network. It also allows us to inform the
        /// client about the current state of all the other currently connected clients so it can start with a
        /// concurrent representation of the server status.
        /// </summary>
//        private void SendClientInitPacket(int connection)
//        {
//            // Buffer needs room for the header as well as all current clients and their client types
//            byte[] buffer = new byte[2 + (ConnectionList.Count * profile.clientInitSubBufferLength)];
//
//            // Goes to PHORIAClient.Receive_NetworkInitialization()
//            buffer[0] = (byte)NetOpFromServer.InitPacket;
//            
//            // The recipients 'true' connectionId
//            buffer[1] = connection;
//
//            return buffer;
//
//            // Populate the rest of the buffer with contextual information about each client depending on the project.
//            for (int i = 0, c = 2; i < ConnectionList.Count; i++, c += profile.poseBufferLength)
//            {
//                AddClientInfoToInitBuffer(ref buffer, i, c);
//            }
//            
//            LogBuffer($"Sending [BeginInitializationHandshake] to [{connection}]", buffer);
//            
//            Send(connection, 1, buffer);
//        }





        protected abstract void Send_ClientConnection(int connection, byte[] buffer);
        
        protected void Send_ClientDisconnected(int connection, byte clientType)
        {
            byte[] buffer = new byte[2];

            buffer[0] = (byte) NetOpFromServer.ClientDisconnected;
            buffer[1] = (byte)connection;
            
            SendToAll(1, buffer);
        }

        #region PNet Event Logs
    
        private void LogPNetEvent(PNetServerEventFlags flag, int connection, byte[] buffer)
        {
            if (!serverEventFlags.HasFlag(flag)) return;
            
            LogPNetEvent(flag.ToString(), connection, buffer);
        }
        
        private void LogPNetEvent(PNetServerEventFlags flag, IEnumerable<int> connections, byte[] buffer)
        {
            if (!serverEventFlags.HasFlag(flag)) return;
            
            LogPNetEvent(flag.ToString(), connections, buffer);
        }
        
        #endregion
        
        protected override void HandleTrafficError(int connection, NetworkError error, UNetEventFlags unetEvent)
        {
            switch (error)
            {
                case NetworkError.WrongHost:
                    break;
                case NetworkError.WrongConnection:
                    break;
                case NetworkError.WrongChannel:
                    break;
                case NetworkError.NoResources:
                    break;
                case NetworkError.BadMessage:
                    break;
                case NetworkError.Timeout:
                    Warning($"A client has timed-out [{connection}].");
                    
                    UNet_Disconnect(connection);
                    break;
                case NetworkError.MessageToLong:
                    break;
                case NetworkError.WrongOperation:
                    break;
                case NetworkError.VersionMismatch:
                    break;
                case NetworkError.CRCMismatch:
                    break;
                case NetworkError.DNSFailure:
                    break;
                case NetworkError.UsageError:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(error), error, null);
            }
        }
    }
}