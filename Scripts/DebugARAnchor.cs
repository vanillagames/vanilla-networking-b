﻿//using UnityEngine;
//using UnityEngine.UI;
//
//using GoogleARCore;
//using GoogleARCore.CrossPlatform;
//using GoogleARCore.Examples.CloudAnchors;
//
//#if UNITY_EDITOR
//using Input = GoogleARCore.InstantPreviewInput;
//#endif
//
//namespace PHORIA.OurPlanet {
//#if UNITY_EDITOR
//	using UnityEditor;
//
//	[CustomEditor(typeof(DebugARAnchor))]
//	public class DebugARAnchorEditor : Editor {
//		DebugARAnchor anchor;
//
//		private void OnEnable() {
//			anchor = (DebugARAnchor)target;
//		}
//
//		public override void OnInspectorGUI() {
//			base.OnInspectorGUI();
//
//			if (Application.isPlaying) {
//				if (GUILayout.Button("Place Anchor")) {
//					anchor.ConfirmAnchorPlacement();
//				}
//			}
//		}
//	}
//#endif
//
//	public class DebugARAnchor : MonoBehaviour {
//
//		public bool useCloudAnchors;
//
//		public ARCoreWorldOriginHelper ARCoreWorldOriginHelper;
//
//		public string cloudAnchorID;
//
//		public Text cloudAnchorIDText;
//
//		Touch touch;
//
//		public Anchor placementAnchor;
//
//		public Transform arAnchorPlacementPreview;
//
//		public GameObject arCrater;
//
//		public OurPlanetPlayerClient client;
//		
//		bool cloudAnchorCreated;
//		bool canCreateAnchor = true;
//
//		public TrackableHit hit;
//
//		public GameObject learnCanvas;
//		public GameObject playCanvas;
//
//		public Button placementButton;
//
//		void Update() {
//			if (Input.touchCount < 1 || (touch = Input.GetTouch(0)).phase != TouchPhase.Moved) {
//				return;
//			}
//
//			PlaceCloudAnchorPreview();
//		}
//
//		public void PlaceCloudAnchorPreview() {
//			if (ARCoreWorldOriginHelper.Raycast(touch.position.x, touch.position.y, TrackableHitFlags.PlaneWithinPolygon, out hit)) {
//				arAnchorPlacementPreview.gameObject.SetActive(true);
//
//				placementButton.interactable = true;
//
//				if (!placementAnchor) {
//					placementAnchor = hit.Trackable.CreateAnchor(hit.Pose);
//				}
//
//				placementAnchor.transform.position = hit.Pose.position;
//				placementAnchor.transform.rotation = hit.Pose.rotation;
//
//				arAnchorPlacementPreview.SetPositionAndRotation(hit.Pose.position, hit.Pose.rotation);
//			} else {
//				arAnchorPlacementPreview.gameObject.SetActive(false);
//
//				placementButton.interactable = false;
//			}
//		}
//
//		public void ConfirmAnchorPlacement() {
//			if (canCreateAnchor) {
//				canCreateAnchor = false;
//
//				learnCanvas.SetActive(false);
//
//				if (useCloudAnchors) {
//					CreateCloudAnchor();
//				} else {
//					PlaceCrater();
//				}
//			}
//		}
//
//		void CreateCloudAnchor() {
//			XPSession.CreateCloudAnchor(placementAnchor).ThenAction(result => {
//				if (result.Response != CloudServiceResponse.Success) {
//					Debug.Log(string.Format("Failed to host Cloud Anchor: {0}", result.Response));
//					cloudAnchorCreated = false;
//					canCreateAnchor = true;
//					learnCanvas.SetActive(true);
//					return;
//				}
//
//				PlaceCrater();
//
//				cloudAnchorCreated = true;
//
//				cloudAnchorID = result.Anchor.CloudId;
//
//				cloudAnchorIDText.text = "Cloud Anchor ID/n[" + cloudAnchorID + "]";
//
//				Debug.Log(string.Format("Cloud Anchor {0} was created and saved.", cloudAnchorID));
//			});
//		}
//
//		void PlaceCrater() {
//			playCanvas.SetActive(true);
//
//			arCrater.transform.SetPositionAndRotation(arAnchorPlacementPreview.position, arAnchorPlacementPreview.rotation);
//			arCrater.SetActive(true);
//
//			arAnchorPlacementPreview.gameObject.SetActive(false);
//
//			canCreateAnchor = false;
//
//			enabled = false;
//
//			client.enabled = true;
//		}
//	}
//}