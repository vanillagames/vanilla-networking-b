﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using PHORIA.Networking;

namespace PHORIA.OurPlanet
{
    public enum PlayCycleState
    {
        None = 0,
        Inactive = 1,
        Waiting = 2,
        Ready = 3,
        Learn = 4,
        Play = 5,
        Act = 6,
        Offboarding = 7
    }
    
    public enum ClientType
    {
        Spectator = 1,
        Manager = 2,
        ClientAndroid = 3,
        ClientWindows = 4,
        Admin = 5
    }

    public enum BiomeType
    {
        Forest,
        Ocean,
        Grasslands,
        Frozen
    }

    public enum NetOpOurPlanet
    {
        ClientBecameReady = 1,
        
        BiomeVoteChange = 2,
        BiomeVoteMajorityChange = 3,
        BiomeVoteLockedIn = 4,
        
        PlayCycleStateChange = 5,
        
    }

//    public enum NetOp_ClientToServer
//    {
//        IsReady = 1,
//        BiomeVoteChange = 2,
//        PledgeSubmission = 3
//    }
//    
//    public enum NetOp_ServerToClient
//    {
//        PlayCycleStateChange = 1,
//        RemoteClientSnapshot = 2
//    }

    public static class OurPlanetByteCodes
    {
//        // ClientTypes ------------------------------------------------------------------------------------------------/
//        
//        public const byte ClientType_Spectator = 1;
//        public const byte ClientType_Manager = 2;
//        public const byte ClientType_Android = 3;
//        public const byte ClientType_Windows = 4;
//        public const byte ClientType_Admin = 5;
//
//        // SessionStatus ----------------------------------------------------------------------------------------------/
//
//        public const byte SessionStatus_None = 0;
//        public const byte SessionStatus_Inactive = 1;
//        public const byte SessionStatus_Waiting = 2;
//        public const byte SessionStatus_Ready = 3;
//        public const byte SessionStatus_Learn = 4;
//        public const byte SessionStatus_Play = 5;
//        public const byte SessionStatus_Act = 6;
//        public const byte SessionStatus_Offboarding = 7;
//
//        // Client -> Server NetOps ------------------------------------------------------------------------------------/
//        
//        public const byte NetOp_ClientToServer_IsReady = 30;
//        public const byte NetOp_ClientToServer_BiomeVoteChange = 31;
//        
//        public const byte NetOp_ClientToServer_SubmitPledge = 40;
//        
//        // Server -> Client NetOps ------------------------------------------------------------------------------------/
//        
//        public const byte NetOp_ServerToClient_StatusChanged = 31;
//        public const byte NetOp_ServerToClient_ServerSnapshot = 32;
    }
    
    public static class OurPlanetHelpers
    {
//        public static DeviceStatus GetStatusFromBufferCode(byte code)
//        {
//            switch (code)
//            {
//                case 0:
//                    return DeviceStatus.Waiting;                    
//                case 1:
//                    return DeviceStatus.Learn;
//                case 2:
//                    return DeviceStatus.Play;
//                default:
//                    throw new FormatException($"Buffer code doesn't correlate to valid DeviceStatus {code}");
//            }
//        }
    }
    
//    [Serializable]
//    public class OurPlanetClientSession : ClientSession
//    {
//        public OurPlanetClientSession(byte connection, byte type) : base(connection, type)
//        {
//            connectionId = connection;
//            clientType = type;
//        }
//
//        public float connectionUptime;
//        
//        [SerializeField]
//        private Transform transform;
//
//        public Transform Transform
//        {
//            get
//            {
//                if (transform != null) return transform;
//                
//                Debug.Log($"Instantiating a new server-side transform to represent the client connection [{connectionId}]");
//                transform = new GameObject($"Remote Client Transform [{connectionId}]").transform;
//
//                return transform;
//            }
//            set => transform = value;
//        }
//        
//        // Cycle-specific analytics go here
//        public List<OurPlanetPlayCycle> Cycles = new List<OurPlanetPlayCycle>();
//    }
    
    public class OurPlanetPlayCycle
    {
        // eg. public int biomeVote;
    }
    
    //-----------------------------------------------------------------------------------------------------------------/
    // Analytics
    //-----------------------------------------------------------------------------------------------------------------/


}