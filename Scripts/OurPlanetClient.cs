﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//
//using UnityEngine;
//
//using PHORIA.Networking;
//
////using OurPlanet;
//
//namespace PHORIA.OurPlanet
//{
//    public class OurPlanetClient : PNetClient
//    {
//        [Header("[ Our Planet Player Client ]")]
//        protected virtual ClientType ClientType { get; }
//
//        [Header(("[ References ]"))]
//        public Transform localClientTransform;
//
//        [Tooltip(("The transform to be inferred as the 'world center' or '0,0,0' for localizing clients positions across multiple unique ARCore sessions."))]
//        public Transform craterTransform;
//
//        public GameObject remoteClientPrefab;
//
//        [Header("[ NetOp Codes ]")]
//        // What was the last position we sent a snapshot of?
//        private Vector3 m_SnapshotPositionDelta;
//
//        // What was the last rotation we sent a snapshot of?
//        private Quaternion m_SnapshotRotationDelta;
//
//        [Header(("[ Pose Thresholding ]"))]
//        public bool thresholdPoseSending = true;
//        
//        // How far away from that delta position do we need to be in order to send a new pose update?
//        public float positionUpdateThreshold = 0.00001f;
//
//        // What angle away from the delta rotation do we need to be in order to trigger a new pose update?
//        public float rotationUpdateThreshold = 0.001f;
//
//        public float positionalDistance;
//        public float rotationalDistance;
//
//        [Header(("[ Remote Client Posing ]"))]
//        private Transform m_RemoteClientCompass;
//
//        private Transform RemoteClientCompass
//        {
//            get
//            {
//                if (m_RemoteClientCompass == null)
//                {
//                    m_RemoteClientCompass = new GameObject().transform;
//                }
//
//                return m_RemoteClientCompass;
//            }
//        }
//
//        [Header(("[ Network Controls ]"))]
//        public bool sendPose = true;
//
//        public bool processServerSnapshot = true;
//
//        private float m_OutgoingTimer, m_OutgoingRate;
//
//        public Dictionary<int, RemoteClientObject> remoteClientObjects;
//
//        public Dictionary<int, RemoteClientObject> RemoteClientObjects => remoteClientObjects ?? (remoteClientObjects = new Dictionary<int, RemoteClientObject>());
//
//        //-------------------------------------------------------------------------------------------------------------/
//        // Overrides
//        //-------------------------------------------------------------------------------------------------------------/
//        
//        protected override void UNet_ConnectToServerSucceeded() {}
//        protected override void UNet_ConnectToServerFailed() {}
//        protected override void UNet_DisconnectFromServerSucceeded() {}
//        protected override void UNet_DisconnectFromServerFailed() {}
//        
//        //-------------------------------------------------------------------------------------------------------------/
//        // Starts
//        //-------------------------------------------------------------------------------------------------------------/
//        
//        void Start()
//        {
//            // According to https://docs.unity3d.com/ScriptReference/Vector3-sqrMagnitude.html
//            // This is the correct way to measure distance using sqrMag.
//            // As long as it only happens once for any given instance...
//            positionUpdateThreshold *= positionUpdateThreshold;
//            m_OutgoingRate = profile.clientSendRate;
//        }
//        
//        //-------------------------------------------------------------------------------------------------------------/
//        // Updates
//        //-------------------------------------------------------------------------------------------------------------/
//        
//        void Update()
//        {
//            IncomingUpdateGate();
//
//            OutgoingTrafficTick();
//        }
//
//        void OutgoingTrafficTick()
//        {
//            m_OutgoingTimer += Time.deltaTime;
//
//            if (!(m_OutgoingTimer >= m_OutgoingRate)) return;
//            
//            m_OutgoingTimer -= m_OutgoingRate;
//            
//            OutgoingUpdateGate();
//        }
//
//        // Nice free way of controlling a send-rate, as long as there are no other FixedUpdate usages/dependencies (physics!)
//        // And there are.
//        
//        // private void FixedUpdate()
//        // {
//        //     HandleOutgoingTraffic();
//        // }
//
//        protected override void NetOp_Send_ClientJoined(int connection, ref byte[] newClientBuffer)
//        {
//            // Pose is 13 bytes plus 3 for the opCode/Connection/ClientType bytes
//            /*
//            Rundown of the outgoing buffer is...
//            0 - opCode
//            1 - connectionId
//            2 - clientType
//            3-15 - pose data
//            */
//            
//            PHORIANetworking.GetCompressedRelativeNetworkPose(ref newClientBuffer, 3, localClientTransform, craterTransform);
//            
//            base.NetOp_Send_ClientJoined(connection, ref newClientBuffer);
//        }
//
////        protected override void Receive_ClientConnected(byte connection, ClientType clientType, byte[] buffer, int dataStart)
////        {
////            base.Receive_ClientConnected(connection, clientType, buffer, dataStart);
////            
////            PHORIANetworking.PoseTransformUsingRelativeBuffer(ClientSessions[connection].Transform, craterTransform, buffer, dataStart);
////        }
//        
//        //-------------------------------------------------------------------------------------------------------------/
//        // Incoming
//        //-------------------------------------------------------------------------------------------------------------/
//
//        protected override void ClientOfTypeConnected(int connection, ClientType clientType, byte[] buffer, int dataStart)
//        {
//            if (connectionId == connection) return;
//
//            switch ((ClientType)clientType)
//            {
//                case ClientType.Spectator:
//                    
//                    break;
//                
//                case ClientType.Manager:
//                    
//                    break;
//                
//                case ClientType.ClientAndroid:
//                    ClientSessions[connection].Transform = Instantiate(remoteClientPrefab).transform;
//
//                    RemoteClientObjects.Add(connection, ClientSessions[connection].Transform.GetComponent<RemoteClientObject>());
//
//                    RemoteClientObjects[connection].SetConnectionId(connection);
//                    break;
//                
//                case ClientType.ClientWindows:
//                    
//                    break;
//                
//                case ClientType.Admin:
//                    
//                    break;
//                
//                default:
//                    throw new ArgumentOutOfRangeException(nameof(clientType), clientType, null);
//            }
//        }
//
//        protected override void ClientOfTypeDisconnected(int connection, ClientType clientType)
//        {
//            switch (clientType)
//            {
//                case ClientType.Spectator:
//                
//                    break;
//                
//                case ClientType.Manager:
//                
//                    break;
//                
//                case ClientType.ClientAndroid:
//                    RemoteClientObjects.Remove(connection);
//                    break;
//
//                case ClientType.ClientWindows:
//                    
//                    break;
//                
//                case ClientType.Admin:
//                
//                    break;
//                
//                default:
//                    throw new ArgumentOutOfRangeException(nameof(clientType), clientType, null);
//            }
//        }
//        
////        protected override bool NetOpCodeReceived(int connection, byte code, byte[] buffer, int receivedSize)
////        {
////            if (base.NetOpCodeReceived(connection, code, buffer, receivedSize)) return true;
////
////            switch (code)
////            {
////                default:
////                    return false;
////                
////                case OurPlanetByteCodes.NetOp_ServerToClient_StatusChanged:
////                    Receive_StatusChange(buffer);
////                    return true;
////                
////                case OurPlanetByteCodes.NetOp_ServerToClient_ServerSnapshot: // <= OurPlanetServer.OutgoingTrafficUpdate()
////                    Receive_ServerSnapshot(buffer, receivedSize);
////                    return true;
////                
////            }
////        }
//        
//        void NetOp_Receive_ServerSnapshot(byte[] buffer, int receivedSize)
//        {
//
//        }
//
//        void Receive_StatusChange(byte[] buffer)
//        {
//            // Invoked by the server when the session status has changed (i.e. Waiting -> Learn)
//            // Buffer position [2] can be considered the outgoing status type (i.e. Waiting in the above example)
//
//            if (buffer.Length != 8)
//            {
//                Error("A buffer of incorrect size has been received");
//            }
//            
//            EventEmitter.Instance.ReceiveOnServerStatusChanged(this, new OnStatusChangedEventArgs
//            {
//                DeviceId = -1,
//                NewStatus = OurPlanetHelpers.GetStatusFromBufferCode(buffer[2]),
//                OldStatus = OurPlanetHelpers.GetStatusFromBufferCode(buffer[3]),
//                SessionIndex = buffer[1]
//            });
//            
//            long timestamp = BitConverter.ToInt64(buffer, 4);
//        }
//        
//        //-------------------------------------------------------------------------------------------------------------/
//        // Outgoing
//        //-------------------------------------------------------------------------------------------------------------/
//
//        protected override void ServerRequestedPassword(int connection, byte[] buffer, int dataSize)
//        {
//            throw new NotImplementedException();
//        }
//
//        protected override void NetOp_Receive_ClientDisconnection()
//        {
//            throw new NotImplementedException();
//        }
//
//        protected override void NetOp_Receive_ClientSnapshotUpdate(int connection, byte[] buffer, int dataStart, int dataEnd)
//        {
//            
//        }
//
//        protected override void NetOp_Receive_ServerSnapshotUpdate(int connection, byte[] buffer, int dataStart, int dataEnd, int subBufferLength)
//        {
//            LogBuffer("Receive_ServerSnapshot", buffer);
//            
//            if (!processServerSnapshot) return;
//            
//            for (int i = dataStart; i < dataEnd; i += subBufferLength)
//            {
//                Log($"starting subBuffer from [{i}]");
//
//                if (ClientSessions.ContainsKey(buffer[i]))
//                {
//                    PHORIANetworking.PoseTransformUsingRelativeBuffer(buffer, i+1, RemoteClientCompass, craterTransform);
//
//                    remoteClientObjects[buffer[i]].targetPosition = RemoteClientCompass.position;
//                    remoteClientObjects[buffer[i]].targetRotation = RemoteClientCompass.rotation;
//                } else if (buffer[i] == connectionId)
//                {
//                    // We aren't re-writing the buffer for each client... sorry guys, you'll have to discard yourselves like this.
//                }
//                else if (buffer[i] == 0)
//                {
//                    // We've hit the remainder of the buffer.
//                    break;
//                }
//                else
//                {
//                    Warning($"There's a connectionId [{buffer[i]}] in this server snapshot that I don't have a record of (and it isn't myself either); how did this happen?");
//                }
//            }
//        }
//
//        protected override void NetOp_Receive_Custom(int connection, byte[] buffer, int dataStart, int dataEnd)
//        {
//            switch ((NetOpOurPlanet) buffer[0])
//            {
//                case NetOpOurPlanet.ClientBecameReady:
//                    break;
//                case NetOpOurPlanet.BiomeVoteChange:
//                    break;
//                case NetOpOurPlanet.BiomeVoteMajorityChange:
//                    break;
//                case NetOpOurPlanet.BiomeVoteLockedIn:
//                    break;
//                case NetOpOurPlanet.PlayCycleStateChange:
//                    break;
//                default:
//                    throw new ArgumentOutOfRangeException();
//            }
//        }
//
////        protected override bool NetOpCodeReceived(int connection, byte code, byte[] buffer, int receivedSize)
////        {
////            throw new NotImplementedException();
////        }
////
////        protected override bool NetOpCodeReceived(int connection, byte code, byte[] buffer)
////        {
////            throw new NotImplementedException();
////        }
//
//        protected override void OutgoingUpdate()
//        {
//            if (!connectionToServerFinalized || !sendPose) return;
//
//            if (thresholdPoseSending)
//            {
//                Vector3 position = localClientTransform.position;
//                Quaternion rotation = localClientTransform.rotation;
//
//                positionalDistance = (position - m_SnapshotPositionDelta).sqrMagnitude;
//                rotationalDistance = Quaternion.Angle(rotation, m_SnapshotRotationDelta);
//
//                m_SnapshotPositionDelta = position;
//                m_SnapshotRotationDelta = rotation;
//
//                if (rotationalDistance < rotationUpdateThreshold && positionalDistance < positionUpdateThreshold)
//                    return;
//            }
//
////            Log($"I'm going to send this packet to {connectionId}");
//
////            byte[] poseBuffer = PHORIANetworking.GetCompressedRelativeNetworkPose(updatePoseCode, (byte) connectionId, localClientTransform, craterTransform);
//            
//            byte[] poseBuffer = new byte[15];
//
//            poseBuffer[0] = (byte) NetOpFromServer.ClientSnapshotUpdate;
//            poseBuffer[1] = (byte) connectionId;
//            
//            PHORIANetworking.GetCompressedRelativeNetworkPose(ref poseBuffer, 2, localClientTransform, craterTransform);
//            
////            LogBuffer("Outgoing pose", poseBuffer);
//            
//            // This is where things get weird.
//            // According to Unity, clients should put their connectionId in the first parameter, while the server puts the respective target clients connection instead.
//            // If a client sends using its connectionId and it isn't [1], it simply says "WrongConnection" and fails to send.
//            // It looks as though clients only consider one connection and thats the one to the server at [1].
//            // This will cause a bit of a re-write elsewhere since I think it gets passed on...
//            // Wherever this lands, the connection should be gotten out of the poseBuffer instead, if it already isn't.
//            
//            // Send(connectionId, 0, poseBuffer);
//
//            LogBuffer("OutgoingTrafficUpdate to [1]", poseBuffer);
//            
//            // This works as expected. UNet is weird.
//            SendToIndividual(1, 0, poseBuffer);
//        }
//
//        public void Send_IsReady()
//        {
//            byte[] buffer = new byte[2];
//            
////            buffer[0] = 
//
//            SendToIndividual(1, 1, buffer);
//        }
//        
//        //--------------------------------------------------------------------------------------------------------------
//        // The new stuff
//        //--------------------------------------------------------------------------------------------------------------
//
//        protected override void SendClientConnectedPacket()
//        {
//            byte[] buffer = new byte[2];
//            
//            buffer[0] = (byte)NetOpFromServer.ClientConnected;
//            buffer[1] = (byte)ClientType;
//            
//            SendToIndividual(1, 1, buffer);
//        }
//    }
//}