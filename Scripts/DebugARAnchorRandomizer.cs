﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugARAnchorRandomizer : MonoBehaviour
{
    private const float posRange = 10.0f;
    
    private void OnEnable()
    {
        transform.position = new Vector3(Random.Range(-posRange, posRange), Random.Range(-posRange, posRange), Random.Range(-posRange, posRange));
        transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
    }
}