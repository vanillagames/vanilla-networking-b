﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using PHORIA.OurPlanet;

namespace PHORIA.Networking
{
    [Flags]
    public enum UNetEventFlags
    {
        Receive = 1, // Every frame NetworkTransport.Receive is called
        Send = 2, // Every frame a NetworkTransport.Send is called
        Connect = 4, // Every frame NetworkTransport.Receive returns NetworkEvent.Connect
        Disconnect = 8, // Every frame NetworkTransport.Receive returns NetworkEvent.Disconnect
        Data = 16, // Every frame NetworkTransport.Receive returns NetworkEvent.Data
        NetworkErrors = 32 // Every time a UNet event returns an errorByte
    }
    
//    [Flags]
//    public enum PNetEventFlags
//    {
//        PasswordRequested = 1,
//        PasswordSubmission = 2,
//        PasswordDenied = 4,
//        
//        IncomingNetOp = 8,
//        OutgoingNetOp = 16,
//        
//        SendInitPacket = 32, // Anytime an InitPacket NetOp is sent
//        
//        ReceiveInitPacket = 64, // Anytime an InitPacket NetOp is received
//        
//        SendClientConnect = 128, // Any time a client sends its ClientConnect buffer
//        ReceiveClientConnect = 256, // Any time a server receives a ClientConnect buffer
//        
//        SendClientDisconnect = 512, // Any time ClientDisconnect runs
//        ReceiveClientDisconnect = 1024, // Any time the 
//        
//        SendToIndividual = 512, // After each standard Send
//        SendToSelected = 1024, // After each SendToList
//        SendToAll = 2048, // After each SendToAll (per-client!)
//        
//        SendBuffer = 4096, // Any outgoing buffer (only once since they're the same per-client)
//        ReceiveBuffer = 8192 // Any incoming buffer
//    }

    [Flags]
    public enum PNetClientEventFlags
    {
        UnconfiguredPacket = 1, // Any received packet that contains a buffer code of 0, which should never happen.
        
        AllIncoming = 2, // All incoming NetOp codes
        AllOutgoing = 4, // All outgoing NetOp codes
        
        StartTrackingConnection = 8, // Start tracking a new client connection
        StopTrackingConnection = 16, // Stop tracking a client connection
        
        ReceivePasswordRequest = 32, // The server has requested a password before allowing access
        SendPasswordSubmission = 64, // We sent the server a password attempt
        ReceiveAccessDenied = 128, // The server has denied access.

        ReceiveInitPacket = 256, // The server has allowed access and sent us initialization information
        
        SendClientConnected = 512, // We tell the server that we're connecting
        ReceiveClientConnected = 1024, // The server tells us that a client has connected (it might be us though!)
        
        SendClientDisconnected = 2048, // We tell the server that we're disconnecting.
        ReceiveClientDisconnected = 4096, // The server tells us that a client has disconnected (it can't be us)
    }

    [Flags]
    public enum PNetServerEventFlags
    {
        UnconfiguredPacket = 1, // Any received packet that contains a buffer code of 0, which should never happen.

        AllIncoming = 2, // All incoming NetOp codes
        AllOutgoing = 4, // All outgoing NetOp codes

        StartTrackingConnection = 8, // Start tracking a new client connection
        StopTrackingConnection = 16, // Stop tracking a client connection

        SendPasswordRequest = 32, // Tell the client that a password is required for access
        ReceivePasswordAttempt = 64, // The client has sent us a password attempt
        SendAccessDenied = 128, // Tell the client we have declined their connection attempt

        SendInitPacket = 256, // Tell the client it can join using the packaged information as setup

        ReceiveClientConnected = 512, // A client has connected
        SendClientConnected = 1024, // Inform all connected clients that another client has connected

        ReceiveClientDisconnected = 2048, // A client has informed us that it is disconnecting
        SendClientDisconnected = 4096, // Inform all connected clients that another client has disconnected
    }



//        PasswordRequested = 1,
//        PasswordSubmission = 2,
//        PasswordDenied = 4,
//        
//        IncomingNetOp = 8,
//        OutgoingNetOp = 16,
//        
//        SendInitPacket = 32, // Anytime an InitPacket NetOp is sent
//        
//        ReceiveInitPacket = 64, // Anytime an InitPacket NetOp is received
//        
//        SendClientConnect = 128, // Any time a client sends its ClientConnect buffer
//        ReceiveClientConnect = 256, // Any time a server receives a ClientConnect buffer
//        
//        SendClientDisconnect = 512, // Any time ClientDisconnect runs
//        ReceiveClientDisconnect = 1024, // Any time the 
//        
//        SendToIndividual = 512, // After each standard Send
//        SendToSelected = 1024, // After each SendToList
//        SendToAll = 2048, // After each SendToAll (per-client!)
//        
//        SendBuffer = 4096, // Any outgoing buffer (only once since they're the same per-client)
//        ReceiveBuffer = 8192 // Any incoming buffer
//    }
    
    public enum NetOpFromServer
    {
        UnconfiguredPacket = 0,
        
        ClientConnected = 255,
        ClientDisconnected = 254,
        
        PasswordRequest = 253,
        PasswordDenied = 252,
        
        InitPacket = 251
    }

    public enum NetOpFromClient
    {
        UnconfiguredPacket = 0,
        Data = 1,

        ClientConnected = 255,
        ClientDisconnected = 254,
        
        PasswordSubmission = 253
    }

//    public abstract class NetworkInitPacket
//    {
//        public virtual int PacketLength { get; }
//
//        public abstract byte[] NewInitPacket();
//    }
            
    public static class PHORIANetworking
    {
        
        public static ShortVector3 ToShortVector3(this Vector3 vector3, float decimalFactor) {
            return new ShortVector3(vector3, decimalFactor);
        }

        public static ShortQuaternion ToShortQuaternion(this Quaternion quaternion, float decimalFactor) {
            return new ShortQuaternion(quaternion, decimalFactor);
        }
        
        /*
        public static byte[] GetCompressedRelativeNetworkPose(byte netOpCode, Transform target, Transform relativityAnchor)
        {
            byte[] buffer = new byte[14];

            buffer[0] = netOpCode;

            Vector3 p = relativityAnchor.InverseTransformPoint(target.position) * 1000.0f;

            BitConverter.GetBytes((short) p.x).CopyTo(buffer, 1);
            BitConverter.GetBytes((short) p.y).CopyTo(buffer, 3);
            BitConverter.GetBytes((short) p.z).CopyTo(buffer, 5);

            ShortQuaternion compressedRotation = Quaternion
                .LookRotation(relativityAnchor.InverseTransformDirection(target.forward))
                .ToShortQuaternion(1000.0f);

            BitConverter.GetBytes(compressedRotation.rA).CopyTo(buffer, 7);
            BitConverter.GetBytes(compressedRotation.rB).CopyTo(buffer, 9);
            BitConverter.GetBytes(compressedRotation.rC).CopyTo(buffer, 11);

            buffer[13] = compressedRotation.rMaxIndex;

            return buffer;
        }
        */
        
        
        /// <summary>
        /// This method will add 13 bytes to the supplied byte array that represent the target transforms position and
        /// rotation, relative to the 'relativityAnchor' transform. This can be used to ensure consistent network poses
        /// even in AR!
        /// </summary>
        /// <param name="buffer">The byte buffer to add the output to.</param>
        /// <param name="startIndex">From which index in the buffer should the data be written to? Make sure the length is sufficient, this will output 13 bytes in total!</param>
        /// <param name="target">Which transforms position and rotation should we encode?</param>
        /// <param name="relativityAnchor">Which transform is acting as the relative point across the network?</param>
        public static void GetCompressedRelativeNetworkPose(ref byte[] buffer, int startIndex, Transform target, Transform relativityAnchor)
        {
            Vector3 p = relativityAnchor.InverseTransformPoint(target.position) * 1000.0f;

            BitConverter.GetBytes((short) p.x).CopyTo(buffer, startIndex);
            BitConverter.GetBytes((short) p.y).CopyTo(buffer, startIndex + 2);
            BitConverter.GetBytes((short) p.z).CopyTo(buffer, startIndex + 4);

            ShortQuaternion compressedRotation = Quaternion
                .LookRotation(relativityAnchor.InverseTransformDirection(target.forward))
                .ToShortQuaternion(1000.0f);

            BitConverter.GetBytes(compressedRotation.rA).CopyTo(buffer, startIndex + 6);
            BitConverter.GetBytes(compressedRotation.rB).CopyTo(buffer, startIndex + 8);
            BitConverter.GetBytes(compressedRotation.rC).CopyTo(buffer, startIndex + 10);

            buffer[startIndex + 12] = compressedRotation.rMaxIndex;
        }
        
        public static void PoseTransformUsingRelativeBuffer(byte[] buffer, int startIndex, Transform target, Transform anchor)
        {
            ShortVector3 positionCache;
            ShortQuaternion rotationCache;

            // Unpack the buffer into ShortVector3 components
            positionCache.x = BitConverter.ToInt16(buffer, startIndex);
            positionCache.y = BitConverter.ToInt16(buffer, startIndex+2);
            positionCache.z = BitConverter.ToInt16(buffer, startIndex+4);

            // Unpack the buffer into ShortQuaternion components
            rotationCache.rA = BitConverter.ToInt16(buffer, startIndex+6);
            rotationCache.rB = BitConverter.ToInt16(buffer, startIndex+8);
            rotationCache.rC = BitConverter.ToInt16(buffer, startIndex+10);
            rotationCache.rMaxIndex = buffer[startIndex+12];

            // Set the remote client to the transform point of the uncompressed Vector3
            target.position = anchor.TransformPoint(positionCache.ToVector3(1000.0f));

            // Set the remote client to the uncompressed Quaternion
            target.rotation = rotationCache.ToQuaternion(1000.0f);

            // With our remote client facing the right direction (?), we can use TransformDirection from the ARAnchor to establish
            // a correct networked forward direction vector
            target.forward = anchor.TransformDirection(target.forward);
        }
    }
    
//    [Serializable]
//    public class ClientSession
//    {
//        public ClientSession(byte connection)
//        {
//            connectionId = connection;
//        }
//
//        [SerializeField]
//        public byte connectionId;
//    }


}
