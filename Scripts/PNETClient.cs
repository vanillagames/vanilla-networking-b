﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using UnityEngine;
using UnityEngine.Networking;

using PHORIA;
using PHORIA.Networking;
using UnityEditor.MemoryProfiler;
using UnityEngine.Events;

namespace PHORIA.Networking
{
    public abstract class PNetClient : NetworkBehaviour
    {
        #region Data

        [Tooltip("Has this NetworkBehaviour connected to a server host socket?")]
        [SerializeField]
        protected bool connectedToServer;

        public UnityEvent OnConnect { get; set; }
        public UnityEvent OnDisconnect { get; set; }

        [Header("[ PNET Client ]")]
        public PNetClientEventFlags clientEventFlags = (PNetClientEventFlags)4095;

        [Tooltip(("This will become true automatically when the client has received an 'InitBuffer' from the server " +
                  "and the client has responded with a ClientConnected buffer of its own. When ClientConnected is " +
                  "received and it matches our connectionId, this bool becomes true."))]
        public bool connectionToServerFinalized;

        protected override byte[] ReceiveBuffer
        {
            get => _receiveBuffer;
            set => _receiveBuffer = new byte[profile.clientReceiveBufferSize];
        }
        
        #endregion
        
        #region Automatic Network Initialization

        public virtual void OnEnable()
        {
            if (!initializeOnEnable) return;

            UNet_InitializeNetworking();
            
            UNet_OpenHostSocket();
            UNet_ConnectToServer();
        }

        public virtual void OnDisable()
        {
            if (!initializeOnEnable) return;

            UNet_DisconnectFromServer();
            UNet_CloseHostSocket();
            
            UNet_DeinitializeNetworking();
        }

        #endregion

        #region Connection Handling
        
        protected override int UNet_ConfigureHostSocket()
        {
            // Clients need to configure a socket for communication it simply looks like this:
            return NetworkTransport.AddHost(profile.topology, 0);
        }

        private void UNet_ConnectToServer()
        {
            if (connectedToServer)
            {
                Warning($"Can't connect - already connected!");
                return;
            }

            connectionId = NetworkTransport.Connect(hostId, profile.ip, profile.localPort, 0, out byte errorByte);

            connectedToServer = !UNetErrorCheck(connectionId, errorByte, UNetEventFlags.Connect);

            if (connectedToServer)
            {
                UNet_ConnectToServerSucceeded();
            }
            else
            {
                UNet_ConnectToServerFailed();
            }
        }

        protected virtual void UNet_ConnectToServerSucceeded()
        {
            Log($"Connected successfully.");
            
        }

        protected virtual void UNet_ConnectToServerFailed()
        {
            Log($"Connection failed.");
        }

        #endregion
        
        #region Disconnection Handling
        
        protected override void UNet_Disconnect(int connection)
        {
            UNet_DisconnectFromServer();
        }
        
        // This should be called when the client wants to gracefully disconnect from the server.
        // This allows us to send a buffer informing the server about the disconnection, rather
        // than simply timing out.
        public void RequestDisconnection()
        {
            SendClientDisconnectedPacket();
            UNet_DisconnectFromServer();
        }
        
        // This function handles the actual UNet disconnection send, as well as a UnityEvent invoke and resetting
        // some important connection settings.
        private void UNet_DisconnectFromServer()
        {
            if (!connectedToServer)
            {
                Warning($"Can't disconnect - not connected to begin with!");
                return;
            }

            if (!networkInitialized)
            {
                Warning($"Can't disconnect - the network isn't initialized to begin with.");
                return;
            }
            
            NetworkTransport.Disconnect(hostId, connectionId, out byte errorByte);
            
            OnDisconnect.Invoke();

            ConnectionList.Clear();

            connectedToServer = false;
            
            UNetErrorCheck(connectionId, errorByte, UNetEventFlags.Disconnect);
        }

        /// <summary>
        /// Send a packet to the server informing it of disconnection with any other useful information first.
        /// </summary>
        protected virtual void SendClientDisconnectedPacket()
        {
            byte[] buffer = new byte[1];

            buffer[0] = (byte) NetOpFromServer.ClientDisconnected;
            
            LogPNetEvent(PNetClientEventFlags.SendClientDisconnected, connectionId, buffer);

            SendToIndividual(hostId, 1, buffer);
        }
       
        #endregion
        
        #region Data & NetOps
        
        protected override void UNet_Data(int connection, byte[] buffer, int dataSize)
        {
            LogPNetEvent(PNetClientEventFlags.AllIncoming, connection, buffer);
            
            switch ((NetOpFromServer) buffer[0])
            {
                case NetOpFromServer.UnconfiguredPacket:
                    LogPNetEvent(PNetClientEventFlags.UnconfiguredPacket, connectionId, buffer);
                    break;

                case NetOpFromServer.PasswordRequest:
                    LogPNetEvent(PNetClientEventFlags.ReceivePasswordRequest, connectionId, buffer);
               	    Receive_ServerRequestedPassword(connection, buffer, dataSize);
                    break;
                
                case NetOpFromServer.PasswordDenied:
                    LogPNetEvent(PNetClientEventFlags.ReceiveAccessDenied, connectionId, buffer);
                    Receive_ServerDeniedPassword(connection, buffer, dataSize);
                    break;
                
                case NetOpFromServer.InitPacket:
                    LogPNetEvent(PNetClientEventFlags.ReceiveInitPacket, connection, buffer);
                    Receive_InitPacket(connection, buffer, dataSize);
                    break;
                
                case NetOpFromServer.ClientConnected:
                    LogPNetEvent(PNetClientEventFlags.ReceiveClientConnected, connection, buffer);
                    ClientConnected(connection, buffer, dataSize);
                    break;
                
                case NetOpFromServer.ClientDisconnected:
                    LogPNetEvent(PNetClientEventFlags.ReceiveClientDisconnected, connection, buffer);
                    Receive_ClientDisconnected(connection, buffer, dataSize);
                    break;
                
                default:
                    Log($"Received NetOp code [{buffer[0]}] did not match any PNetClient events.");
                    break;
            }
        }
        
        #endregion

        #region NetOps

        protected virtual void Receive_ServerRequestedPassword(int connection, byte[] buffer, int dataSize)
        {
            Send_Password("Fuzzy pickles");
        }

        /// <summary>
        /// Used for replying to a PHORIAServers ServerRequestedPassword NetOp call. This should probably be invoked
        /// from a UI element like a button or InputField and remain public in order to do so.
        /// </summary>
        /// <param name="passwordAttempt">The string to send to the server.</param>
        public virtual void Send_Password(string passwordAttempt)
        {
            bufferConstructor.Clear();
            
            bufferConstructor.Add((byte)NetOpFromClient.PasswordSubmission);
            
            bufferConstructor.AddRange(Encoding.UTF8.GetBytes(passwordAttempt));

            byte[] buffer = bufferConstructor.ToArray();
            
            LogPNetEvent(PNetClientEventFlags.SendPasswordSubmission, connectionId, buffer);
            
            SendToIndividual(hostId,1,  buffer);
        }

        protected virtual void Receive_ServerDeniedPassword(int connection, byte[] buffer, int dataSize)
        {
            LogPNetEvent(PNetClientEventFlags.ReceiveAccessDenied, connection, buffer);
        }
        
        protected virtual void Receive_InitPacket(int connection, byte[] incomingBuffer, int dataSize)
        {
            connectionId = incomingBuffer[1];
            
            SendClientConnectedPacket();
        }

        /// <summary>
        /// This buffer should contain all the information necessary to instantiate this client across the network
        /// for both the server and other clients.
        /// </summary>
        protected virtual void SendClientConnectedPacket()
        {
            byte[] buffer = new byte[1];

            buffer[0] = (byte) NetOpFromServer.ClientConnected;
            
            LogPNetEvent(PNetClientEventFlags.SendClientConnected, connectionId, buffer);
            
            SendToIndividual(hostId, 1, buffer);
        }

        protected override void ClientConnected(int connection, byte[] buffer, int dataSize)
        {
            if (buffer[1] == connectionId)
            {
                // It's us!
                connectionToServerFinalized = true;
                
                OnConnect.Invoke();
                return;
            } else {
                LogPNetEvent(PNetClientEventFlags.StartTrackingConnection, connection, buffer);
                
                StartTrackingClient(buffer[1]);
            }
        }

        protected override void Receive_ClientDisconnected(int connection, byte[] buffer, int dataSize)
        {
            StopTrackingClient(connection);
        }

        #endregion
        
        #region PNet Event Logs
    
        private void LogPNetEvent(PNetClientEventFlags flag, int connection, byte[] buffer)
        {
            if (!clientEventFlags.HasFlag(flag)) return;
            
            LogPNetEvent(flag.ToString(), connection, buffer);
        }
        
        private void LogPNetEvent(PNetClientEventFlags flag, IEnumerable<int> connections, byte[] buffer)
        {
            if (!clientEventFlags.HasFlag(flag)) return;
            
            LogPNetEvent(flag.ToString(), connections, buffer);
        }
        
        #endregion
        
        #region Errors and Exceptions

        protected override void HandleTrafficError(int connection, NetworkError error, UNetEventFlags unetEvent)
        {
            switch (error)
            {
                case NetworkError.Ok:
                    break;
                case NetworkError.WrongHost:
                    
                    break;
                case NetworkError.WrongConnection:
                    break;
                case NetworkError.WrongChannel:
                    break;
                case NetworkError.NoResources:
                    break;
                case NetworkError.BadMessage:
                    break;
                case NetworkError.Timeout:
                    Warning("The connection to the server has timed-out.");
                    UNet_DisconnectFromServer();
                    break;
                case NetworkError.MessageToLong:
                    break;
                case NetworkError.WrongOperation:
                    break;
                case NetworkError.VersionMismatch:
                    break;
                case NetworkError.CRCMismatch:
                    break;
                case NetworkError.DNSFailure:
                    break;
                case NetworkError.UsageError:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(error), error, null);
            }
        }
        
        #endregion
    }
}