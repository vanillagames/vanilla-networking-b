﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class RelativePositionDebugger : MonoBehaviour
{
    public Text text;
    public Transform client;
    public Transform crater;
    
    
    
    void Update()
    {
        text.text = $"Pos [{crater.InverseTransformPoint(client.position)}]";
    }
}
