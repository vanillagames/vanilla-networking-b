﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;

namespace PHORIA.Networking
{
	public abstract class NetworkBehaviour : PHORIABehaviour
	{
		#region Data

		[Header("[ Network Behaviour ]")]
		public NetworkProfile profile;

		[EnumFlags(2)]
		public UNetEventFlags uNetEventFlags = (UNetEventFlags)7;
		
		[Header("Settings")]
		[Tooltip(("If true, the NetworkBehaviour will attempt to initialize NetworkTransport the moment the" +
		          " component is enabled and de-initialize when the component is disabled."))]
		public bool initializeOnEnable = true;
		
		[Header("Network Status")] [Tooltip("Is this NetworkBehaviour connected to the network?")]
		public bool networkInitialized;

		[Tooltip("Has this network behaviour opened a host for communication? The socket configuration will be" +
		         " different for clients and servers.")]
		public bool hostOpen;

		[Header("Network Identity")]
		[Tooltip(("Which host are we currently connected to? For most situations, this will only ever be 0 and can" +
		          " largely be ignored. The number increments whenever a host is added and is a purely independent" +
		          " and unsynchronized value."))]
		public int hostId = -1;

		[Tooltip("What is this NetworkBehaviours connectionId?")]
		public int connectionId = -1;

		[SerializeField, Header(("Connected Clients"))]
		private List<int> connectedClients;

		protected List<int> ConnectionList => connectedClients ?? (connectedClients = new List<int>());

		[Header("Traffic")]
		private NetworkEventType m_IncomingNetworkEvent;
		
		public float outgoingSendRate = 0.125f;

		private float outgoingSendTickdown = 0;

		protected byte[] _receiveBuffer;

		protected virtual byte[] ReceiveBuffer
		{
			get => _receiveBuffer;
			set => _receiveBuffer = value;
		}
		
		[SerializeField]
		private List<byte> _bufferConstructor;
		protected List<byte> bufferConstructor => _bufferConstructor ?? (_bufferConstructor = new List<byte>());	
		#endregion
		
		#region Networking Initialization

		///-----------------------------------------------------------------------------------------------------------/
		/// UNet Initialization
		///-----------------------------------------------------------------------------------------------------------/
		///
		/// These two calls are fundamental to networking with NetworkTransport.
		/// In fact, they're so fundamental that its safe to simply invoke them in OnEnable and OnDisable without
		/// any foreseeable trouble. Attempting any network operations without them in place is very dangerous
		/// and can produce unexpected results.
		///
		///-----------------------------------------------------------------------------------------------------------/
		
		protected void UNet_InitializeNetworking()
		{
			if (networkInitialized)
			{
				Warning($"Networking is already initialized.");
				return;
			}

			Log("Initializing UNET.");

			NetworkTransport.Init(profile.globalConfig);

			networkInitialized = true;
		}

		protected void UNet_DeinitializeNetworking()
		{
			if (!networkInitialized)
			{
				Warning($"Networking is already uninitialized.");
				return;
			}

			Log("De-initializing UNET.");

			NetworkTransport.Shutdown();

			networkInitialized = false;
		}

		protected void UNet_OpenHostSocket()
		{
			if (hostOpen)
			{
				Warning($"Can't open host socket - it's already open!");
				return;
			}

			hostId = UNet_ConfigureHostSocket();

			hostOpen = true;
		}

		protected abstract int UNet_ConfigureHostSocket();

		protected void UNet_CloseHostSocket()
		{
			if (!hostOpen)
			{
				Warning($"Can't close host socket - it isn't open to begin with!");
				return;
			}

			NetworkTransport.RemoveHost(0);

			hostId = -1;

			hostOpen = false;
		}
		#endregion

		#region Updates
		
		///-----------------------------------------------------------------------------------------------------------/
		/// Updates
		///-----------------------------------------------------------------------------------------------------------/
		///
		/// These functions are intended to be run in some kind of repeating call (Update, FixedUpdate,
		/// InvokeRepeating, etc).
		/// 
		/// The frequency should be managed using outgoingSendRate, but any implementation will work the same.
		///
		///-----------------------------------------------------------------------------------------------------------/

		/// This ensures that IncomingUpdate is called only if its viable to do so.
		protected void IncomingUpdateGate()
		{
			if (!networkInitialized || !hostOpen) return;

			IncomingUpdate();
		}
		
		private void IncomingUpdate()
		{
			bool trafficRemaining = true;
			
			// How many UNet events have been received thus far this frame?
			// UNet events stack up whenever they aren't immediately processed, kind of like squeezing a hose.
			// In certain situations, like returning after pausing the app, this may lead to a frame
			// with lots of events all at once. While useful, it might be a good idea to throttle this on a 
			// per-channel basis down the line. For example, only the newest event on the unreliable ('Update')
			// channel is processed, but all on the reliable ('Event') channel are processed.
			int networkEventCount = -1;

			while (trafficRemaining)
			{
				networkEventCount++;

				// Get a new 'Receive' buffer.
				// This buffer needs to be at least the size of the biggest expected packet across the whole app.
				// If the buffer is smaller than the received packet, the MessageToLong (sic) error will be thrown
				// by UNet.
				ReceiveBuffer = new byte[profile.defaultIncomingBufferSize];

				// Establish which kind of the NetworkEvent enum has been received and handle it seperately.
				m_IncomingNetworkEvent = NetworkTransport.Receive
				(
					out int host,
					out int connection,
					out int channel,
					ReceiveBuffer,
					ReceiveBuffer.Length,
					out int receivedSize,
					out byte errorByte
				);

				// If the check returns true, an error has been detected and we drop out early.
				if (UNetErrorCheck(connection, errorByte, UNetEventFlags.Receive)) return;
				
				// Is it possible for this to actually happen? [Untested]
				if (connection == 0) return;
				
				// Handle each event seperately
				switch (m_IncomingNetworkEvent)
				{
					case NetworkEventType.Nothing:
						// Cancel the above while loop the moment we hit an 'empty' UNet event.
						trafficRemaining = false;
						break;
					case NetworkEventType.ConnectEvent:
						LogUNetEvent(UNetEventFlags.Connect, connection);
						UNet_Connect(connection);
						break;
					case NetworkEventType.DisconnectEvent:
						LogUNetEvent(UNetEventFlags.Disconnect, connection);
						UNet_Disconnect(connection);
						break;
					case NetworkEventType.DataEvent:
						LogUNetEvent(UNetEventFlags.Data, connection);
						
						if (_receiveBuffer.Length != 0) 
							UNet_Data(connection, _receiveBuffer, receivedSize);
						else
							Warning($"Discarding an empty buffer received from connection [{connection}]");							
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}

			// It might be important to note when more than one message has been received in a given frame.
			// This is relatively normal, especially regarding the connection or disconnection stage, but
			// probably shouldn't be happening afterwards unless the game is very network heavy.
			// Ideally, all server information will be contained in a single Update packet.
			if (networkEventCount > 1)
			{
				Log($"We had more than one event this frame: [{networkEventCount}]");
			}
		}
		
		// This function is intended to be run in Update or on some kind of timer.
		// The rate at which this runs should be controlled with outgoingSendRate acting as the minimum time between
		// OutgoingUpdate. OutgoingUpdate will never be called twice in one frame using this method, so outgoingSendRate
		// can safely be set to 0 for the fastest possible send rate.
		protected void OutgoingUpdateGate()
		{
			if (!networkInitialized || !hostOpen) return;

			outgoingSendTickdown = Mathf.Repeat(outgoingSendRate + Time.deltaTime, 100.0f);

			if (outgoingSendTickdown < outgoingSendRate) return;
            
			outgoingSendTickdown -= outgoingSendRate;
			
			OutgoingUpdate();
		}
		
		protected abstract void OutgoingUpdate();
		
		#endregion

		#region UNet Events

		///------------------------------------------------------------------------------------------------------------------------------------------/
		/// NetworkTransport - Receive
		///------------------------------------------------------------------------------------------------------------------------------------------/

		/// <summary>
		/// This function occurs when UNet receives a connection packet across the network.
		///
		/// Unfortunately, the data provided with this event does not allow for network connectivity and no extra
		/// data can be sent with it. As a result, we do a follow-up 'handshake' network event of our own in order
		/// to properly establish some context about the new connection, including sending them their correct
		/// connectionId and allowing the client to send back a generic/interpretable 'clientType' id, intended for
		/// distinguishing the platform or application type the connection is coming from.
		/// </summary>
		/// <param name="connection"></param>
		protected virtual void UNet_Connect(int connection) {}

		protected virtual void UNet_Disconnect(int connection) {}

		protected abstract void UNet_Data(int connection, byte[] buffer, int dataSize);

		#endregion

		#region Sending

		/// <summary>
		/// This function will send the given byte[] across the network. The recipientId should be different depending on if this is being called by a server/host or a client. A server inputs the clients connectionID while a client inputs its own.
		/// </summary>
		/// <param name="connection">If coming from a client, this should be the clients personal connectionID. If its coming from the server, it should be the desired clients connectionID.</param>
		/// <param name="channelId">Which channel should be used to send this data?</param>
		/// <param name="buffer">The contents of the data packet itself, packaged into bytes so it can be sent over the network.</param>
		protected void SendToIndividual(int connection, int channelId, byte[] buffer)
		{
			// We shouldn't be able to send a packet if there's no socket open or if the outgoing buffer is empty.
			if (!hostOpen || buffer.Length == 0) return;

			LogUNetEvent(UNetEventFlags.Send, connection);

			NetworkTransport.Send(hostId, connection, channelId, buffer, buffer.Length, out byte sendErrorByte);

			UNetErrorCheck(connection, sendErrorByte, UNetEventFlags.Send);
		}

		protected void SendToSelected(int channelId, byte[] buffer, IEnumerable<int> connections)
		{
			if (!hostOpen || buffer.Length == 0) return;

			foreach (int connection in connections)
			{
				LogUNetEvent(UNetEventFlags.Send, connection);

				NetworkTransport.Send(hostId, connection, channelId, buffer, buffer.Length, out byte sendErrorByte);

				UNetErrorCheck(connection, sendErrorByte, UNetEventFlags.Send);
			}
		}
		
		protected void SendToAll(int channelId, byte[] buffer)
		{
			if (!hostOpen || buffer.Length == 0) return;

			foreach (int connection in ConnectionList)
			{
				LogUNetEvent(UNetEventFlags.Send, connection);

				NetworkTransport.Send(hostId, connection, channelId, buffer, buffer.Length, out byte sendErrorByte);

				UNetErrorCheck(connection, sendErrorByte, UNetEventFlags.Send);
			}
		}
		
		#endregion

		#region Client Management

		/// <summary>
		/// This should be called when a client is considered officially connected to the server. It should be
		/// reserved for functionality like instantiating a player prefab or adding the client to a public UI list,
		/// using the contents of the buffer for project-contextual information.
		/// </summary>
		/// <param name="connection">Which client connected?</param>
		/// <param name="buffer">An agnostic set of data provided by the client across the network. It can
		/// be interpreted in any fashion.</param>
		/// <param name="dataSize">At which index of the buffer does the packaged information end? The rest of
		/// the buffer will be populated by 0s, the default for a byte.</param>
		
		protected void StartTrackingClient(int connection)
		{
			if (ConnectionList.Contains(connection))
			{
				Warning($"This connection can't be added because its already present [{connection}]");
				return;
			}
			
			ConnectionList.Add(connection);
		}
		
		protected void StopTrackingClient(int connection)
		{
			if (!ConnectionList.Contains(connection))
			{
				Warning($"This connection {connection} cannot be removed because it isn't present to begin with.");
				return;
			}
			
			ConnectionList.Remove(connection);
		}
		
		// This event is considered the definitive moment at which a client can be considered connected for the context
		// of the application. Unlike UNet_Connect, this event contains a contextual buffer allowing the client to bring
		// with it any kind of information it needs.
		protected abstract void ClientConnected(int connection, byte[] buffer, int dataSize);

		protected abstract void Receive_ClientDisconnected(int connection, byte[] buffer, int dataSize);

		protected abstract void Receive_ClientDisconnectedAbrupt(int connection);

		#endregion
		
		#region Logging

		private void LogUNetEvent(UNetEventFlags eventType, int connection)
		{
			if (uNetEventFlags.HasFlag(eventType))
			{
				Log($"UNet event - [{eventType}] Connection [{connection}]");
			}
		}

		protected void LogPNetEvent(string eventString, int connection, byte[] buffer)
		{
			string bufferString = buffer.Aggregate(string.Empty, (current, t) => current + $" [{t}]");

			Log($"PNet event - [{eventString}] Connection [{connection}]\nBuffer contents {bufferString}");
		}
		
		protected void LogPNetEvent(string eventString, IEnumerable<int> connections, byte[] buffer)
		{
			string connectionString = connections.Aggregate(string.Empty, (current, t) => current + $" [{t}]");
			string bufferString = buffer.Aggregate(string.Empty, (current, t) => current + $" [{t}]");

			Log($"PNet event - [{eventString}] Connections {connectionString}\nBuffer contents {bufferString}");
		}
		
		protected bool UNetErrorCheck(int connection, byte errorByte, UNetEventFlags eventType)
		{
			// If the errorByte is 0 ('NetworkError.Ok'), return that no error was detected.
			if (errorByte == 0)
			{
				return false;
			}
			else
			{
				// If the errorByte was anything else, it's some kind of error and we should log it.
				if (uNetEventFlags.HasFlag(UNetEventFlags.NetworkErrors))
				{
					Error($"UNet [{eventType}] error - [{(NetworkError) errorByte}]");
				}
				
				HandleTrafficError(connection, (NetworkError) errorByte, eventType);
				
				return true;
			}
		}
		
		protected abstract void HandleTrafficError(int connection, NetworkError error, UNetEventFlags unetEvent);

		#endregion
	}
}