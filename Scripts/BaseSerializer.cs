﻿using System.IO;

using System.Runtime.Serialization.Formatters.Binary;

using UnityEngine;

namespace PHORIA.Serialization {
	public abstract class BaseSerializer<T> {

		public enum SerializationMethod {
			BinaryFormatter,
			BytePacking
		}

		[SerializeField]
		public SerializationMethod serializationMethod;

		[SerializeField]
		public int cacheByteCount;

		[SerializeField]
		public byte[] cacheInput;

		[SerializeField]
		public T cacheOutput;

		public void SerializeToInputCache(T payload) {
			cacheInput = Serialize(payload);
			cacheByteCount = cacheInput.Length;
		}

		public byte[] Serialize(T payload) {
			if (payload == null) {
				return null;
			}

			switch (serializationMethod) {
				case SerializationMethod.BinaryFormatter:
					BinaryFormatter bf = new BinaryFormatter();

					using (MemoryStream ms = new MemoryStream()) {
						bf.Serialize(ms, payload);

						return ms.ToArray();
					}

				case SerializationMethod.BytePacking:
					return BytePackingSerialization(payload);

				default:
					return null;
			}
		}

		public void DeserializeToOutputCache() {
			cacheOutput = Deserialize(cacheInput);
		}

		public T Deserialize(byte[] bytes) {
			if (bytes == null || bytes.Length == 0) {
				return default;
			}

			switch (serializationMethod) {
				case SerializationMethod.BinaryFormatter:
					BinaryFormatter formatter = new BinaryFormatter();
					using (MemoryStream ms = new MemoryStream()) {
						ms.Write(bytes, 0, bytes.Length);
						ms.Seek(0, SeekOrigin.Begin);
						return (T)formatter.Deserialize(ms);
					}

				case SerializationMethod.BytePacking:
					return BytePackingDeserialization(bytes);

				default:
					return default;
			}
		}

		public virtual byte[] BytePackingSerialization() {
			return null;
		}

		public virtual T BytePackingDeserialization() {
			return default;
		}

		public virtual byte[] BytePackingSerialization(T payload) {
			return null;
		}

		public virtual T BytePackingDeserialization(byte[] bytes) {
			return default;
		}
	}
}