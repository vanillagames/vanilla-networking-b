﻿//using System.Collections;
//using System.Collections.Generic;
//
//using UnityEngine;
//
//using PHORIA.Networking;
//using UnityEditor.PackageManager;
//
//namespace PHORIA.OurPlanet
//{
//    /// <summary>
//    /// This client is responsible for driving the audio/visual output for an Our Planet session. It is intended to run
//    /// on the same device running an Our Planet server.
//    /// </summary>
//    public class OurPlanetSpectatorClient : PNetClient
//    {
////        protected override byte ClientTypeCode => 1;
//        protected override ClientType ClientType => ClientType.Spectator;
//
//        private void Update()
//        {
//            IncomingUpdateGate();
//        }
//
//
//        protected override void UNet_ConnectToServerSucceeded()
//        {
//            
//        }
//
//        protected override void UNet_ConnectToServerFailed()
//        {
//            
//        }
//
//        protected override void UNet_DisconnectFromServerSucceeded()
//        {
//            
//        }
//
//        protected override void UNet_DisconnectFromServerFailed()
//        {
//            
//        }
//
//        protected override void ServerRequestedPassword(int connection, byte[] buffer, int dataSize)
//        {
//            throw new System.NotImplementedException();
//        }
//
//        protected override void NetOp_Receive_ClientDisconnection()
//        {
//            throw new System.NotImplementedException();
//        }
//
//        protected override void NetOp_Receive_ClientSnapshotUpdate(int connection, byte[] buffer, int dataStart, int dataEnd)
//        {
//            throw new System.NotImplementedException();
//        }
//
//        protected override void NetOp_Receive_Custom(int connection, byte[] buffer, int dataStart, int dataEnd)
//        {
//            throw new System.NotImplementedException();
//        }
//
//        protected override void OutgoingUpdate()
//        {
//            throw new System.NotImplementedException();
//        }
//
//        protected override void ClientOfTypeConnected(int connection, ClientType clientType, byte[] buffer, int dataStart)
//        {
//            if (clientType == ClientType.ClientAndroid)
//            {
//                
//            }
//        }
//
//        protected override void ClientOfTypeDisconnected(int connection, ClientType clientType)
//        {
//            
//        }
//    }
//}