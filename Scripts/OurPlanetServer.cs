﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//
////using GoogleARCore;
//
//using UnityEngine;
//using UnityEngine.Networking;
//
//using PHORIA;
//using PHORIA.Networking;
//
////using RotaryHeart.Lib.SerializableDictionary;
//
//namespace PHORIA.OurPlanet
//{
//    [Serializable]
////    public class OurPlanetClientSessionDictionary : SerializableDictionaryBase<int, OurPlanetClientSession> {}
//    
//    public class OurPlanetServer : PNetServer
//    {
////        protected override int NewClientInitBufferLength => 2;
////        protected override int ClientInitInfoLength => 15;
//        
//
//
//
//        [SerializeField]
////        private OurPlanetClientSessionDictionary ourPlanetSessions;
////        private OurPlanetClientSessionDictionary OurPlanetSessions => ourPlanetSessions ?? (ourPlanetSessions = new OurPlanetClientSessionDictionary());
//
//        private Dictionary<int, OurPlanetClientSession> ourPlanetSessions;
//        private Dictionary<int, OurPlanetClientSession> OurPlanetSessions => ourPlanetSessions ?? (ourPlanetSessions = new Dictionary<int, OurPlanetClientSession>());
//
//        [Tooltip("A list of all connections that should receive snapshot updates from the server. Certain clients may want to connect without necessarily hearing about all player activity.")]
//        public List<int> updateableClients = new List<int>();
//        
//        private Transform clientTransformCache;
//        
//        private ShortVector3 positionCache;
//        private ShortQuaternion rotationCache;
//
////        [Tooltip("How long should we wait before sending a new server snapshot to the clients?")]
////        public float snapshotSendThreshold = 4.0f;
//        
//        // How far away from that delta position do we need to be in order to send a new pose update?
//        public float positionUpdateThreshold = 0.00001f;
//
//        // What angle away from the delta rotation do we need to be in order to trigger a new pose update?
//        public float rotationUpdateThreshold = 0.001f;
//
//        public bool spectatorConnected;
//        private int spectatorConnection = -1;
//        
//        public bool managerConnected;
//        private int managerConnection = -1;
//
//        private const byte k_SnapshotOpCode = 10;
//
//        public List<byte> snapshotBuffer;
//
//        public List<byte> SnapshotBuffer
//        {
//            get => snapshotBuffer ?? (snapshotBuffer = new List<byte> {k_SnapshotOpCode});
//            set => snapshotBuffer = value;
//        }
//
//        private void Awake()
//        {
//            positionUpdateThreshold *= positionUpdateThreshold;
//        }
//        
//        void Update()
//        {
//            IncomingUpdateGate();
//        }
//        
//        private void FixedUpdate()
//        {
//            OutgoingUpdateGate();
//        }
//        
//        protected override void AddClientInfoToInitBuffer(ref byte[] buffer, int currentClientId, int currentBufferIndex)
//        {
//            base.AddClientInfoToInitBuffer(ref buffer, currentClientId, currentBufferIndex);
//            
//            PHORIANetworking.GetCompressedRelativeNetworkPose(ref buffer, 3 + currentBufferIndex,ClientSessions[ConnectionList[currentClientId]].Transform, transform);
//        }
//
//        protected override void Send_ClientConnection(int connection, byte[] buffer)
//        {
//            throw new NotImplementedException();
//        }
//
//        protected override void ServerRequestedPassword(int connection, byte[] buffer, int dataSize)
//        {
//            throw new NotImplementedException();
//        }
//
//        protected override void NetOp_Receive_InitializationHandshake(int connection, byte[] buffer, int dataStart, int dataEnd)
//        {
//            throw new NotImplementedException();
//        }
//
//        protected override void NetOp_Receive_ClientDisconnection()
//        {
//            throw new NotImplementedException();
//        }
//        
//        protected override void NetOp_Receive_ClientSnapshotUpdate(int connection, byte[] buffer, int dataStart, int dataEnd)
//        {
//            LogBuffer($"Receive_UpdateClientTransform from [{connection}]", buffer);
//
//            if (!OurPlanetSessions.ContainsKey(connection))
//            {
//                Warning($"There is no session for this connection [{connection}] yet");
//                return;
//            }
//
//            if (!OurPlanetSessions[connection].Transform)
//            {
//                Warning($"The transform representing this connection [{connection}] doesn't exist; it probably hasn't been instantiated yet.");
//                return;
//            }
//            
//            // Cache the transform
//            clientTransformCache = OurPlanetSessions[connection].Transform;
//
//            // Cache the delta position/rotation
//            Vector3 deltaPos = clientTransformCache.position;
//            Quaternion deltaRot = clientTransformCache.rotation;
//            
//            // Decompress and apply the new pose to the transform
//            PHORIANetworking.PoseTransformUsingRelativeBuffer(buffer, 1, clientTransformCache, transform);
//                       
//            // If the delta distance of this client from its last update is below the updateThreshold distance and the angle between its last rotation update is also less than the rotationUpdate distance, don't mark this client pose as dirty.
//            if ((clientTransformCache.position - deltaPos).sqrMagnitude < positionUpdateThreshold && Quaternion.Angle(clientTransformCache.rotation, deltaRot) < rotationUpdateThreshold)
//            {
//                Log("Packet discarded for not being a significant local change");
//                return;
//            }
//
//            Log($"Adding significant pose update from client {connection} to the snapshotBuffer.");
//            
//            // Simply add the buffer onto the end of snapshotBuffer.
//            // Because we know buffer is always 16 bytes, clients can safely iterate through them 16 indices at a time
//            // to make sense of the data.
////            snapshotBuffer.AddRange(buffer);
//
//            // Add the connection as a byte
//            snapshotBuffer.Add((byte)connection);
//            
//            // Add the pose buffer, ignoring the netOpCode at the start
//            snapshotBuffer.AddRange(new ArraySegment<byte>(buffer, 1, 13));
//            
//            LogBuffer($"Receive_UpdateClientTransform - SnapshotBuffer state at end-of-function", buffer);
//        }
//
//        protected override void NetOp_Receive_Custom(int connection, byte[] buffer, int dataStart, int dataEnd)
//        {
//            switch ((NetOpOurPlanet) buffer[0])
//            {
//                case NetOpOurPlanet.ClientBecameReady:
//                    
//                    break;
//                
//                case NetOpOurPlanet.BiomeVoteChange:
//                    
//                    break;
//                
//                case NetOpOurPlanet.BiomeVoteMajorityChange:
//                    
//                    break;
//                
//                case NetOpOurPlanet.BiomeVoteLockedIn:
//                    
//                    break;
//                
//                case NetOpOurPlanet.PlayCycleStateChange:
//                    
//                    break;
//                
//                default:
//                    throw new ArgumentOutOfRangeException();
//            }
//        }
//
////        protected override bool NetOpCodeReceived(int connection, byte code, byte[] buffer, int receivedSize)
////        {
////            throw new NotImplementedException();
////        }
//
////        protected override void Send_ClientConnected(int connection, byte clientType, byte[] incomingBuffer, int dataStart)
////        {
////            byte[] outgoingBuffer = new byte[3];
////
////            outgoingBuffer[0] = (byte)NetOpNative.NewClientConnected; // Goes to PHORIAClient.Receive_ClientConnected()
////            outgoingBuffer[1] = (byte) connection;
////            outgoingBuffer[2] = clientType;
////            
////            outgoingBuffer[3]
////            
////            LogBuffer($"Send_ClientConnected to [All]", outgoingBuffer);
////
////            SendToAll(1, outgoingBuffer);
////        }
//
//        protected override void ClientOfTypeConnected(int connection, ClientType clientType, byte[] buffer, int dataStart)
//        {
//            if (connectionId == connection)
//            {
//                Warning($"This connection {connection} matched the servers connectedId so it was rejected!");
//                return;
//            }
//            
//            switch (clientType)
//            {
//                case ClientType.ClientAndroid:
//                    Log("Oh joy, a new player client! I'll add them to the Session dictionary...");
//                    
//                    // A player client connected...
//                    OurPlanetSessions.Add(connection, new OurPlanetClientSession(connection, clientType));
//                    
//                    // Pose the new client relative to the crater
//                    PHORIANetworking.PoseTransformUsingRelativeBuffer(buffer, 3,  ClientSessions[connection].Transform, transform);
//                    
//                    updateableClients.Add(connection);
//                    break;
//                case ClientType.Spectator:
//                    // The spectator connected...
//                    spectatorConnected = true;
//                    spectatorConnection = connection;
//                    
//                    updateableClients.Add(connection);
//                    break;
//                case ClientType.Manager:
//                    // The management app connected...
//
//                    managerConnected = true;
//                    managerConnection = connection;
//                    break;
//                case ClientType.ClientWindows:
//                    // A windows test client connected...
//                    break;
//                
//                case ClientType.Admin:
//                    // An admin client has connected...
//                    break;
//            }
//        }
//
//        protected override void ClientOfTypeDisconnected(int connection, ClientType clientType)
//        {
//            switch (clientType)
//            {
//                case ClientType.ClientAndroid:
//                    //OurPlanetSessions[connection].SaveOrSendSomewhereWhoKnows();
//                    OurPlanetSessions.Remove(connection);
//                    
//                    // Destroy Remote Client Prefab here!
//                    updateableClients.Remove(connection);
//                    break;
//                case ClientType.Spectator:
//                    // The spectator disconnected...
//                    spectatorConnected = false;
//                    spectatorConnection = -1;
//                    
//                    updateableClients.Remove(connection);
//                    break;
//                case ClientType.Manager:
//                    // The management app disconnected...
//
//                    managerConnected = false;
//                    managerConnection = -1;
//                    break;
//                case ClientType.ClientWindows:
//                    // A windows test client disconnected...
//                    break;
//                
//                case ClientType.Admin:
//                    // An admin client has disconnected...
//                    break;
//            }
//        }
//       
////        protected override bool NetOpCodeReceived(int connection, byte code, byte[] buffer, int receivedSize)
////        {
//////            LogFunction("{OurPlanetServer - NetOpCodeReceived}");
////
////            if (base.NetOpCodeReceived(connection, code, buffer, receivedSize)) return true;
////
////            switch (code)
////            {
////                case 2: // From OurPlanetClient.OutgoingTrafficUpdate()
////                    Receive_UpdateClientTransform(connection, buffer);
////                    return true;
////                
////                case 10: // User changed their Biome vote
////                    Receive_UpdateClientsBiomeVote(connection, buffer);
////                    return true;
////                
////                default:
////                    return false;
////            }
////        }
//
////        protected override bool NetOpCodeReceived(int connection, byte code, byte[] buffer)
////        {
////            throw new NotImplementedException();
////        }
//
//        void Receive_UpdateClientTransform(int connection, byte[] buffer)
//        {
//            LogBuffer($"Receive_UpdateClientTransform from [{connection}]", buffer);
//
//            if (!OurPlanetSessions.ContainsKey(connection))
//            {
//                Warning($"There is no session for this connection [{connection}] yet");
//                return;
//            }
//
//            if (!OurPlanetSessions[connection].Transform)
//            {
//                Warning($"The transform representing this connection [{connection}] doesn't exist; it probably hasn't been instantiated yet.");
//                return;
//            }
//            
//            // Cache the transform
//            clientTransformCache = OurPlanetSessions[connection].Transform;
//
//            // Cache the delta position/rotation
//            Vector3 deltaPos = clientTransformCache.position;
//            Quaternion deltaRot = clientTransformCache.rotation;
//            
//            // Decompress and apply the new pose to the transform
//            PHORIANetworking.PoseTransformUsingRelativeBuffer(buffer, 1, clientTransformCache, transform);
//                       
//            // If the delta distance of this client from its last update is below the updateThreshold distance and the angle between its last rotation update is also less than the rotationUpdate distance, don't mark this client pose as dirty.
//            if ((clientTransformCache.position - deltaPos).sqrMagnitude < positionUpdateThreshold && Quaternion.Angle(clientTransformCache.rotation, deltaRot) < rotationUpdateThreshold)
//            {
//                Log("Packet discarded for not being a significant local change");
//                return;
//            }
//
//            Log($"Adding significant pose update from client {connection} to the snapshotBuffer.");
//            
//            // Simply add the buffer onto the end of snapshotBuffer.
//            // Because we know buffer is always 16 bytes, clients can safely iterate through them 16 indices at a time
//            // to make sense of the data.
////            snapshotBuffer.AddRange(buffer);
//
//            // Add the connection as a byte
//            snapshotBuffer.Add((byte)connection);
//            
//            // Add the pose buffer, ignoring the netOpCode at the start
//            snapshotBuffer.AddRange(new ArraySegment<byte>(buffer, 1, 13));
//            
//            LogBuffer($"Receive_UpdateClientTransform - SnapshotBuffer state at end-of-function", buffer);
//        }
//        
//        protected override void OutgoingUpdate()
//        {
//            if (updateableClients.Count == 0 || snapshotBuffer.Count < 2)
//            {
//                return;
//            }
//            
//            LogBuffer("ServerSnapshotBuffer", snapshotBuffer.ToArray());
//            
//            // Using the unreliable channel, send the snapshotBuffer to all the clients listed in 'updateableClients'.
//            SendToSelected(0, snapshotBuffer.ToArray(), updateableClients);
//            
//            // Reset the snapshotBuffer
//            snapshotBuffer.Clear();
//            
//            // Add the opCode for the next time
//            snapshotBuffer.Add(k_SnapshotOpCode);
//            
//            LogBuffer("Fresh ServerSnapshotBuffer?", snapshotBuffer.ToArray());
//        }
//
//        protected override void ClientConnected(int connection, byte[] buffer, int dataSize)
//        {
//            
//        }
//
//        protected override void Receive_ClientDisconnected(int connection, byte[] buffer, int dataSize)
//        {
//            
//        }
//
//        protected override void Receive_ClientDisconnectedAbrupt(int connection)
//        {
//            
//        }
//
//        protected void Receive_UpdateClientsBiomeVote(int connection, byte[] buffer)
//        {
//            
//        }
//    }
//}