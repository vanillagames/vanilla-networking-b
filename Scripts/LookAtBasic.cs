﻿using UnityEngine;

public class LookAtBasic : MonoBehaviour {

	public Transform target;

	private void OnEnable()
	{
		target = Camera.main.transform;
	}

	public virtual void Update() {
		if (target) {
			transform.LookAt(target);
		}
	}
}