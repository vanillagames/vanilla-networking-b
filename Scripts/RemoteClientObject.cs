﻿using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace PHORIA.Networking
{
	public class RemoteClientObject : ClientObject
	{
		public Vector3 targetPosition;
		public Quaternion targetRotation;

		public const float positionSmoothing = 8.0f;
		public const float rotationSmoothing = 8.0f;
		
		void Update()
		{
			transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * positionSmoothing);
			transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * rotationSmoothing);
		}

		public void SetConnectionId(int connection)
		{
			connectionId = connection;

			GetComponentInChildren<Text>().text = connection.ToString();
		}
	}
}