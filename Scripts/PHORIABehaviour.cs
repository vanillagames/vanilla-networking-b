﻿using System;

using UnityEngine;

namespace PHORIA {
	public class PHORIABehaviour : MonoBehaviour {
		[Flags]
		public enum DebugFlags {
			Logs = 1,
			Warnings = 2,
			Errors = 4,
			Gizmos = 8,
		}

		[Header("[ PHORIA Behaviour ]")]
		[EnumFlags(4)]
		public DebugFlags debugFlags = (DebugFlags)31;

		protected void Log(string message) {
			if (debugFlags.HasFlag(DebugFlags.Logs))
				Debug.Log($"[{gameObject.name}] [{Time.frameCount}] {message}");
		}

		protected void Warning(string message) {
			if (debugFlags.HasFlag(DebugFlags.Warnings))
				Debug.LogWarning($"[{gameObject.name}] [{Time.frameCount}] {message}");
		}

		protected void Error(string message) {
			if (debugFlags.HasFlag(DebugFlags.Errors))
				Debug.LogError($"[{gameObject.name}] [{Time.frameCount}] {message}");
		}
	}
}