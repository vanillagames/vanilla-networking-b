﻿using System;

using UnityEngine;
using UnityEngine.Networking;

namespace PHORIA.Networking
{
	[Serializable, CreateAssetMenu(fileName = "New Network Connection Profile", menuName = "PHORIA/Networking/Connection Profile")]
	public class NetworkProfile : ScriptableObject {
		public int maxNumberOfConnections = 32;

		[Tooltip("How much time needs to elapse before a new Update packet is sent? The default is 0.033333f which is 30fps.")]
		public float clientSendRate = 0.033333f;
		public float serverSendRate = 0.033333f;

		[Tooltip("How many Updates should be sent per second? ")]
		[Range(1, 60)]
		[SerializeField]
		public int sendsPerSecond;

		[HideInInspector]
//		public float SendRate => 1.0f / sendsPerSecond;
		public float SendRate => (1.0f / sendsPerSecond) * Time.deltaTime; // This I think? Test time!

		public int localPort = 26000; // Has to match server

		public int webPort = 26001; // Has to match server

		public string ip = "localhost";  // Has to match server

		public int defaultIncomingBufferSize = 1024;
		
		public int serverReceiveBufferSize = 64;
		public int clientReceiveBufferSize = 512;

		[SerializeField]
		public GlobalConfig globalConfig;

		[SerializeField]
		public HostTopology topology;
		
//		[Tooltip(("When a new client connects and we want to tell them information about the state of the server," +
//		          "how many bytes are used at the start of the buffer to describe the world state? For simple games" +
//		          " this may be small, but some games want to include things like the current server time or player" +
//		          " scores."))]
//		public int initBufferLength = 1;
				
		[Tooltip(("When a new client connects and we want to tell them information about the other clients already" +
		          "connected, how many bytes are used to cover a single client? This is used so the server and clients" +
		          " can know how long to make their byte buffers when reading and writing."))]

		/*
		0 - clients connection Id
		1 - clients clientType
		2-14 - clients pose
		*/
		public int clientInitSubBufferLength = 15;

		/*
		0 - clients connection Id
		1-13 - clients pose
		*/
		public int clientPoseUpdateBufferLength = 14;

		[Tooltip("How many bytes are used in an InitBuffer for this project? The InitBuffer is the first packet a" +
		         " client receives from the server if access is granted and contains all information the client" +
		         " requires to become concurrent with the state of the network.")]
		public int initBufferSize = 2;
	}
}